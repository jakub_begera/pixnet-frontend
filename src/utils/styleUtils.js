import styled from "styled-components";

export const ErrorWrapper = styled.div`
  width: 100%;
  padding: 1em;
`;