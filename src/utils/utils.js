/**
 * Perform given delay in ms.
 * @param timeout - time in ms
 * @returns {Promise<any>}
 */
export const delayAsync = async (timeout) => await new Promise((resolve) => setTimeout(resolve, timeout));
