export function getConnectionStatusLabel(status) {
    switch (status) {
        case "ONLINE":
            return "success";
        case "OFFLINE":
            return "danger";
        case "CONNECTING":
            return "warning";
        default:
            return "default";
    }
}

export function getMeasurementStatusLabel(status) {
    switch (status) {
        case "primary":
            return "success";
        case "MEASURING":
            return "success";
        default:
            return "default";
    }
}

export function getHandlerStateLabel(status) {
    switch (status) {
        case "ONLINE":
            return "success";
        case "OFFLINE":
            return "default";
        case "ERROR":
            return "danger";
        default:
            return "default";
    }
}