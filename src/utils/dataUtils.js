export function formatValueUnit(unit) {
    switch (unit) {
        case "NANO_SECOND": return "ns";
        case "SECOND": return "s";
        case "VOLT": return "V";
        case "CELSIUS": return "°C";
        default: return null;
    }
}