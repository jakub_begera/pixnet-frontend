import {IS_PRODUCTION} from "../constants";
import {buildUrl} from "build-url";

export function buildHandlerUrl(path, queryParams) {
    return buildUrl(IS_PRODUCTION ? "" : "http://localhost:8080", {
        path: `api/${path}`,
        queryParams: queryParams
    });
}