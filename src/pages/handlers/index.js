import React, {Component} from 'react';
import {Button, ButtonToolbar, Grid, PageHeader} from "react-bootstrap";
import AlertsHolder from "../../components/AlertsHolder";
import HandlersTable from "./HandlersTable";
import AddHandlerModal from "./AddHandlerModal";

class Handlers extends Component {

    state = {
        alertsHolder: React.createRef(),
        handlersTable: React.createRef(),
        isAddModalVisible: false
    };

    render() {
        return (
            <Grid>

                <PageHeader>
                    Handlers
                </PageHeader>

                <AlertsHolder ref={this.state.alertsHolder}/>

                <ButtonToolbar>

                    <Button bsStyle="primary"
                            onClick={() => this.setState({isAddModalVisible: true})}
                    >
                        Add
                    </Button>

                    <Button
                        onClick={() => this.state.handlersTable.current.fetchItems()}
                    >
                        reload
                    </Button>

                </ButtonToolbar>

                <br/>

                <HandlersTable
                    ref={this.state.handlersTable}
                    addErrorAlert={(message) => this.state.alertsHolder.current.addAlert(message, "danger")}
                    addWarningAlert={(message) => this.state.alertsHolder.current.addAlert(message, "warning")}
                    history={this.props.history}
                />

                <AddHandlerModal
                    show={this.state.isAddModalVisible}
                    onHide={() => this.setState({isAddModalVisible: false})}
                    onSuccess={() => {
                        this.setState({isAddModalVisible: false});
                        this.state.handlersTable.current.fetchItems();
                    }}
                    onError={(message) => {
                        this.setState({isAddModalVisible: false});
                        this.state.alertsHolder.current.addAlert(message, "danger")
                    }}
                />

            </Grid>
        );
    }
}

export default Handlers;
