import React, {Component} from 'react';
import {Button, ControlLabel, FormControl, FormGroup, Modal} from "react-bootstrap";
import Loader from "../../components/Loader";
import {buildHandlerUrl} from "../../utils/api";

class AddHandlerModal extends Component {

    state = {
        name: "",
        host: "",
        port: 0,
        subnetworkID: -1,
        loading: false,
        subnetworks: []
    };

    getNameValidationState() {
        const length = this.state.name.length;
        if (length > 3) return 'success';
        else if (length > 0) return 'error';
        return null;
    }

    getHostValidationState() {
        const length = this.state.host.length;
        if (length > 3) return 'success';
        else if (length > 0) return 'error';
        return null;
    }

    getPortValidationState() {
        const port = Number(this.state.port);
        if (Number.isInteger(port) && port > 0 && port <= 65535) {
            return 'success';
        } else {
            return 'error';
        }
    }

    isFormValid() {
        const port = Number(this.state.port);
        return this.state.name.length > 3
            && this.state.host.length > 3
            && Number.isInteger(port) && port > 0 && port <= 65535
            && this.state.subnetworkID !== -1
    }

    async fetchSubnetworks() {
        this.setState({loading: true});

        let error = null;
        try {
            const response = await fetch(buildHandlerUrl("subnetwork/getAll"));
            let json = await response.json();

            if (json) {
                if (json.success) {
                    if (json.data && json.data.length > 0) {
                        this.setState({
                            subnetworks: json.data,
                            subnetworkID: json.data[0].id
                        });
                    } else {
                        error = "No available subnetworks were found. Did you add some?";
                    }

                } else {
                    error = "Request fail (" + response.status + "): " + json.message;
                }
            } else {
                error = "Request fail (" + response.status + ")";
            }

        } catch (e) {
            error = e.message;
        }

        if (error) {
            this.props.onError(`Adding of handler was unsuccessful. Unable to fetch subnetworks: ${error}.`);
        }

        this.setState({loading: false});
    }

    async submit() {
        this.setState({loading: true});

        try {
            const response = await fetch(buildHandlerUrl("handler/add"), {
                method: 'POST',
                body: JSON.stringify({
                    name: this.state.name,
                    subnetworkID: this.state.subnetworkID,
                    host: this.state.host,
                    port: Number(this.state.port)
                }),
                headers: {
                    "Content-Type": "application/json; charset=utf-8"
                }
            });
            let json = await response.json();

            if (json && json.success !== true) {
                this.props.onError(`Adding of handler was unsuccessful. (${json.message})`);
            } else {
                this.props.onSuccess();
            }
        } catch (e) {
            console.error("Adding of handler was unsuccessful");
            this.props.onError(`Adding of handler was unsuccessful. (${e.message})`);
        }

        this.setState({loading: false});
    }

    dismiss() {
        this.setState({loading: false});
        this.props.onHide()
    }

    async componentDidMount() {
        await this.fetchSubnetworks();
    }

    render() {

        const isFormValid = this.isFormValid();

        return (
            <Modal
                {...this.props}
                bsSize="large"
                onHide={() => this.dismiss()}
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">Add handler</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    {
                        this.state.loading
                            ? <Loader/>
                            : <form>

                                {/* Name */}
                                <FormGroup
                                    controlId="formBasicText"
                                    validationState={this.getNameValidationState()}>
                                    <ControlLabel>Handler name</ControlLabel>
                                    <FormControl
                                        type="text"
                                        value={this.state.name}
                                        placeholder="Enter name of new handler"
                                        onChange={(e) => this.setState({name: e.target.value})}
                                    />
                                    <FormControl.Feedback/>
                                </FormGroup>

                                {/* Host */}
                                <FormGroup
                                    controlId="formBasicText"
                                    validationState={this.getHostValidationState()}>
                                    <ControlLabel>Host</ControlLabel>
                                    <FormControl
                                        type="text"
                                        value={this.state.host}
                                        placeholder="Host server address"
                                        onChange={(e) => this.setState({host: e.target.value})}
                                    />
                                    <FormControl.Feedback/>
                                </FormGroup>

                                {/* Port */}
                                <FormGroup
                                    controlId="formBasicText"
                                    validationState={this.getPortValidationState()}>
                                    <ControlLabel>Port</ControlLabel>
                                    <FormControl
                                        type="txtNumber"
                                        value={this.state.port}
                                        placeholder="Host server port"
                                        onChange={(e) => this.setState({port: e.target.value})}
                                    />
                                    <FormControl.Feedback/>
                                </FormGroup>

                                <FormGroup controlId="formControlsSelect">
                                    <ControlLabel>Subnetwork</ControlLabel>
                                    <FormControl
                                        componentClass="select"
                                        placeholder="select"
                                        onChange={(e) => this.setState({subnetworkID: e.target.value})}>
                                        {this.state.subnetworks.map(item =>
                                            <option key={item.id} value={item.id}>{item.name}</option>
                                        )}
                                    </FormControl>
                                </FormGroup>

                            </form>
                    }

                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={() => this.dismiss()}>Cancel</Button>
                    <Button disabled={this.state.loading || !isFormValid} bsStyle="primary"
                            onClick={() => this.submit()}>Add</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default AddHandlerModal;