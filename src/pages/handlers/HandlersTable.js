import React, {Component} from "react";
import Loader from "../../components/Loader";
import {Alert, Button, ButtonToolbar, Col, Grid, Label, Panel, PanelGroup, Row, Table} from "react-bootstrap";
import {buildHandlerUrl} from "../../utils/api";
import {ErrorWrapper} from "../../utils/styleUtils";
import {COLOR_ACCENT} from "../../utils/colors";
import {BeatLoader} from "react-spinners";
import {getConnectionStatusLabel, getHandlerStateLabel, getMeasurementStatusLabel} from "../../utils/viewStateUtils";

class HandlersTable extends Component {

    state = {
        items: [],
        isLoading: false,
        error: null
    };

    async fetchItems(showLoading = true) {
        if (showLoading) this.setState({isLoading: true});
        try {
            const response = await fetch(buildHandlerUrl("handler/getAll"));
            let json = await response.json();

            if (json) {
                if (json.success) {
                    this.setState({
                        items: json.data,
                        error: null
                    });
                } else {
                    this.setState({
                        items: [],
                        error: "Request fail (" + response.status + "): " + json.message
                    });
                }
            } else {
                this.setState({
                    items: [],
                    error: "Request fail (" + response.status + ")"
                });
            }

        } catch (e) {
            this.setState({
                items: [],
                error: e.message
            });
        }
        if (showLoading) this.setState({isLoading: false});
    }

    async removeHandler(id) {
        console.log("Removing handler with ID: " + id);

        const index = this.state.items.findIndex(item => item.id === id);
        let items = this.state.items;
        items[index].loading = true;

        this.setState({
            items: items
        });

        try {
            const response = await fetch(buildHandlerUrl("handler?id=" + id), {
                method: 'DELETE'
            });
            let json = await response.json();

            if (json && json.success === false) {
                this.props.onError(`Removal of handler with ID ${id} was unsuccessful. (${json.message})`);
            }
            await this.fetchItems(false)
        } catch (e) {
            console.error("Removing handler with ID " + id + " was unsuccessful");
            this.props.addErrorAlert(`Removal of handler with ID ${id} was unsuccessful. (${e.message})`);
        }
    }

    goToDetectorDetail(id) {
        this.props.history.push(`detectors/${id}`)
    }

    async componentDidMount() {
        await this.fetchItems();
    }

    render() {
        const {isLoading, error, items} = this.state;

        if (isLoading) {
            return (<Loader/>)
        }


        if (error) {
            return (
                <ErrorWrapper>
                    <Alert bsStyle="danger"><strong>Handlers loading error: </strong>{error}</Alert>
                </ErrorWrapper>
            )
        }

        if (items.length === 0) {
            return (
                <ErrorWrapper>
                    <Alert bsStyle="info">No handler found.</Alert>
                </ErrorWrapper>
            )
        }

        return items.map(item =>
            <Panel bsStyle={getHandlerStateLabel(item.state)}>
                <Panel.Heading>{item.name}</Panel.Heading>
                <Panel.Body>
                    <Grid fluid={true}>

                        {item.error &&
                        <Row>
                            <Alert bsStyle="danger">{item.error}</Alert>
                        </Row>
                        }

                        <Row>
                            {/* Overview */}
                            <Col md={4} sm={12}>
                                <Table hover responsive>
                                    <tbody>
                                    <tr>
                                        <td><strong>ID:</strong></td>
                                        <td>{item.id}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Name:</strong></td>
                                        <td>{item.name}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>State:</strong></td>
                                        <td>
                                            <Label
                                                bsStyle={getHandlerStateLabel(item.state && "UNKNOWN")}>
                                                {item.state && "UNKNOWN"}
                                            </Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Host:</strong></td>
                                        <td>{item.host}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Port:</strong></td>
                                        <td>{item.port}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Subnetwork:</strong></td>
                                        <td>{`${item.subnetwork.name} (#${item.subnetwork.id})`}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Version:</strong></td>
                                        <td>{item.version}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Updated at:</strong></td>
                                        <td>
                                            {item.lastUpdatedAt
                                                ? new Date(item.lastUpdatedAt * 1000).toISOString()
                                                : "never"
                                            }
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Action:</strong></td>
                                        <td>
                                            {item.loading
                                                ? <BeatLoader size={8} color={COLOR_ACCENT}/>
                                                : <ButtonToolbar>
                                                    <Button
                                                        bsStyle="danger"
                                                        bsSize="xsmall"
                                                        onClick={() => this.removeHandler(item.id)}
                                                    >
                                                        Remove
                                                    </Button>
                                                </ButtonToolbar>
                                            }
                                        </td>
                                    </tr>
                                    </tbody>
                                </Table>
                            </Col>

                            {/* Detectors */}
                            <Col md={8} sm={12}>
                                <h3>Bound detectors</h3>
                                {item.detectors.length > 0
                                    ? <PanelGroup accordion>
                                        {item.detectors.map((detector, index) =>
                                            <Panel key={index} eventKey={index} bsStyle="info">
                                                <Panel.Heading>
                                                    <Panel.Title toggle>{detector.name}</Panel.Title>
                                                </Panel.Heading>
                                                <Panel.Body collapsible>
                                                    <ButtonToolbar>
                                                        <Button
                                                            bsStyle="primary"
                                                            bsSize="xsmall"
                                                            onClick={() => this.goToDetectorDetail(detector.id)}
                                                        >
                                                            Detail
                                                        </Button>
                                                    </ButtonToolbar>
                                                    <br/>
                                                    <Table hover responsive>
                                                        <tbody>
                                                        <tr>
                                                            <td><strong>ID:</strong></td>
                                                            <td>{detector.id}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Name:</strong></td>
                                                            <td>{detector.name}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Connection status:</strong></td>
                                                            <td>
                                                                <Label
                                                                    bsStyle={getConnectionStatusLabel(
                                                                        detector.status && detector.status.connection
                                                                            ? detector.status.connection
                                                                            : "UNKNOWN"
                                                                    )}>
                                                                    {
                                                                        detector.status && detector.status.connection
                                                                            ? detector.status.connection
                                                                            : "UNKNOWN"
                                                                    }
                                                                </Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Measurement status:</strong></td>
                                                            <td>
                                                                <Label
                                                                    bsStyle={getMeasurementStatusLabel(
                                                                        detector.status && detector.status.measurement
                                                                            ? detector.status.measurement
                                                                            : "UNKNOWN"
                                                                    )}>
                                                                    {
                                                                        detector.status && detector.status.measurement
                                                                            ? detector.status.measurement
                                                                            : "UNKNOWN"
                                                                    }
                                                                </Label>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </Table>
                                                </Panel.Body>
                                            </Panel>
                                        )}
                                    </PanelGroup>
                                    : <Alert bsStyle="warning">No bound detectors.</Alert>
                                }
                            </Col>
                        </Row>
                    </Grid>
                </Panel.Body>
            </Panel>
        )
    }
}

export default HandlersTable;