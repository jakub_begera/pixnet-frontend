import React, {Component} from "react";
import Loader from "../../components/Loader";
import {Alert, Button, ButtonToolbar, Col, Grid, Label, Panel, Row, Table} from "react-bootstrap";
import {buildHandlerUrl} from "../../utils/api";
import {BeatLoader} from "react-spinners";
import {COLOR_ACCENT} from "../../utils/colors";
import {getConnectionStatusLabel, getMeasurementStatusLabel} from "../../utils/viewStateUtils";
import {ErrorWrapper} from "../../utils/styleUtils";
import styled from "styled-components";

const PanelHeadingHover = styled(Panel.Heading)`
  cursor: pointer;
`;

class DetectorsTable extends Component {

    state = {
        items: [],
        isLoading: false,
        error: null
    };

    async fetchItems(showLoading = true) {
        if (showLoading) this.setState({isLoading: true});
        try {
            const response = await fetch(buildHandlerUrl("detector/getAll"));
            let json = await response.json();

            if (json) {
                if (json.success) {
                    this.setState({
                        items: json.data,
                        error: null
                    });
                } else {
                    this.setState({
                        items: [],
                        error: "Request fail (" + response.status + "): " + json.message
                    });
                }
            } else {
                this.setState({
                    items: [],
                    error: "Request fail (" + response.status + ")"
                });
            }

        } catch (e) {
            this.setState({
                items: [],
                error: e.message
            });
        }
        if (showLoading) this.setState({isLoading: false});
    }

    async removeDetector(id) {
        console.log("Removing detector with ID: " + id);

        const index = this.state.items.findIndex(item => item.id === id);
        let items = this.state.items;
        items[index].loading = true;

        this.setState({
            items: items
        });

        try {
            const response = await fetch(buildHandlerUrl("detector?id=" + id), {
                method: 'DELETE'
            });
            let json = await response.json();

            if (json && json.success === false) {
                this.props.onError(`Removal of detector with ID ${id} was unsuccessful. (${json.message})`);
            }
            await this.fetchItems(false)
        } catch (e) {
            console.error("Removing detector with ID " + id + " was unsuccessful");
            this.props.addErrorAlert(`Removal of handler with ID ${id} was unsuccessful. (${e.message})`);
        }
    }

    async componentDidMount() {
        await this.fetchItems();
    }

    goToDetectorDetail(id) {
        this.props.history.push(`detectors/${id}`)
    }

    render() {
        const {isLoading, error, items} = this.state;

        if (isLoading) {
            return (<Loader/>)
        }


        if (error) {
            return (
                <ErrorWrapper>
                    <Alert bsStyle="danger"><strong>Handlers loading error: </strong>{error}</Alert>
                </ErrorWrapper>
            )
        }

        if (items.length === 0) {
            return (
                <ErrorWrapper>
                    <Alert bsStyle="info">No detectors found.</Alert>
                </ErrorWrapper>
            )
        }

        return items.map(item =>
            <Panel key={item.id} bsStyle={getConnectionStatusLabel(item.status.connection)}>
                <PanelHeadingHover onClick={() => this.goToDetectorDetail(item.id)}>{item.name}</PanelHeadingHover>
                <Panel.Body>
                    <Grid fluid={true}>

                        {item.error &&
                        <Row>
                            <Alert bsStyle="danger">{item.error}</Alert>
                        </Row>
                        }

                        <Row>
                            <Col md={4} sm={12}>
                                <Table hover responsive>
                                    <tbody>
                                    <tr>
                                        <td><strong>ID:</strong></td>
                                        <td>{item.id}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Name:</strong></td>
                                        <td>{item.name}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Connection status:</strong></td>
                                        <td>
                                            <Label
                                                bsStyle={getConnectionStatusLabel(
                                                    item.status && item.status.connection
                                                        ? item.status.connection
                                                        : "UNKNOWN"
                                                )}>
                                                {
                                                    item.status && item.status.connection
                                                        ? item.status.connection
                                                        : "UNKNOWN"
                                                }
                                            </Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Measurement status:</strong></td>
                                        <td>
                                            <Label
                                                bsStyle={getMeasurementStatusLabel(
                                                    item.status && item.status.measurement
                                                        ? item.status.measurement
                                                        : "UNKNOWN"
                                                )}>
                                                {
                                                    item.status && item.status.measurement
                                                        ? item.status.measurement
                                                        : "UNKNOWN"
                                                }
                                            </Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Subnetwork:</strong></td>
                                        <td>{`${item.subnetwork.name} (#${item.subnetwork.id})`}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Bound handler:</strong></td>
                                        <td>{item.handler
                                            ? `${item.handler.name} (#${item.handler.id})`
                                            : `No handler`
                                        }</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Action:</strong></td>
                                        <td>
                                            {item.loading
                                                ? <BeatLoader size={8} color={COLOR_ACCENT}/>
                                                : <ButtonToolbar>
                                                    <Button
                                                        bsStyle="primary"
                                                        bsSize="xsmall"
                                                        onClick={() => this.goToDetectorDetail(item.id)}
                                                    >
                                                        Detail
                                                    </Button>
                                                    <Button
                                                        bsStyle="danger"
                                                        bsSize="xsmall"
                                                        onClick={() => this.removeDetector(item.id)}
                                                    >
                                                        Remove
                                                    </Button>
                                                </ButtonToolbar>
                                            }
                                        </td>
                                    </tr>
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </Grid>
                </Panel.Body>
            </Panel>
        )
    }
}

export default DetectorsTable;