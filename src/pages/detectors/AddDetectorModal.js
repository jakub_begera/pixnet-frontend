import React, {Component} from 'react';
import {Button, ControlLabel, FormControl, FormGroup, Modal} from "react-bootstrap";
import Loader from "../../components/Loader";
import {buildHandlerUrl} from "../../utils/api";
import AlertsHolder from "../../components/AlertsHolder";


class AddDetectorModal extends Component {

    state = {
        alertsHolder: React.createRef(),
        loading: false,
        subnetworks: [],
        id: "",
        name: "",
        subnetworkID: -1,
        configFile: null,
        commPluginFile: null,
        dataPersistencePluginFile: null,
    };

    resetState() {
        this.setState({
            id: "",
            name: "",
            subnetworkID: -1,
            configFile: null,
            commPluginFile: null,
            dataPersistencePluginFile: null,
        });
    }

    getIdValidationState() {
        // const regexp = new RegExp('/^\\S*$/');
        const length = this.state.id.length;
        if (length > 3) return 'success';
        else if (length > 0) return 'error';
        return null;
    }

    getNameValidationState() {
        const length = this.state.name.length;
        if (length > 3) return 'success';
        else if (length > 0) return 'error';
        return null;
    }


    isFormValid() {
        return this.state.id.length > 3
            && this.state.name.length > 3
            && this.state.subnetworkID !== -1
            && this.state.configFile
            && this.state.commPluginFile
            && this.state.dataPersistencePluginFile
    }

    async fetchSubnetworks() {
        this.setState({loading: true});

        let error = null;
        try {
            const response = await fetch(buildHandlerUrl("subnetwork/getAll"));
            let json = await response.json();

            if (json) {
                if (json.success) {
                    if (json.data && json.data.length > 0) {
                        this.setState({
                            subnetworks: json.data,
                            subnetworkID: json.data[0].id
                        });
                    } else {
                        error = "No available subnetworks were found. Did you add some?";
                    }

                } else {
                    error = "Request fail (" + response.status + "): " + json.message;
                }
            } else {
                error = "Request fail (" + response.status + ")";
            }

        } catch (e) {
            error = e.message;
        }

        if (error) {
            this.props.onError(`Unable to fetch subnetworks: ${error}.`);
        }

        this.setState({loading: false});
    }

    async submit() {
        this.setState({loading: true});

        try {

            const url = buildHandlerUrl("detector/add", {
                id: this.state.id,
                name: this.state.name,
                subnetwork_id: this.state.subnetworkID
            });

            const formData = new FormData();
            formData.append('config', this.state.configFile);
            formData.append('plugin_comm', this.state.commPluginFile);
            formData.append('plugin_data_persistence', this.state.dataPersistencePluginFile);

            const options = {
                method: 'POST',
                body: formData,
            };

            let response = await fetch(url, options);
            let json = await response.json();

            if (json && json.success !== true) {
                this.state.alertsHolder.current
                    .addAlert(`Adding of detector was unsuccessful. (${json.message})`, "danger")
            } else {
                this.resetState();
                this.props.onSuccess();
            }
        } catch (e) {
            console.error("Adding of detector was unsuccessful");
            this.state.alertsHolder.current.addAlert(`Adding of detector was unsuccessful. (${e.message})`, "danger")
        }

        this.setState({loading: false});
    }

    dismiss() {
        this.setState({loading: false});
        this.resetState();
        this.props.onHide()
    }

    async componentDidMount() {
        await this.fetchSubnetworks();
    }

    render() {

        const isFormValid = this.isFormValid();

        return (
            <Modal
                {...this.props}
                bsSize="large"
                onHide={() => this.dismiss()}
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">Add detector</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <AlertsHolder ref={this.state.alertsHolder}/>
                    {
                        this.state.loading
                            ? <Loader/>
                            : <div>
                                <form>

                                    {/* ID */}
                                    <FormGroup
                                        controlId="formBasicText"
                                        validationState={this.getIdValidationState()}>
                                        <ControlLabel>ID</ControlLabel>
                                        <FormControl
                                            type="text"
                                            value={this.state.id}
                                            placeholder="Detector ID"
                                            onChange={(e) => this.setState({id: e.target.value})}
                                        />
                                        <FormControl.Feedback/>
                                    </FormGroup>


                                    {/* Name */}
                                    <FormGroup
                                        controlId="formBasicText"
                                        validationState={this.getNameValidationState()}>
                                        <ControlLabel>Handler name</ControlLabel>
                                        <FormControl
                                            type="text"
                                            value={this.state.name}
                                            placeholder="Enter a name of new detector"
                                            onChange={(e) => this.setState({name: e.target.value})}
                                        />
                                        <FormControl.Feedback/>
                                    </FormGroup>

                                    {/* Subnetwork */}
                                    <FormGroup controlId="formControlsSelect">
                                        <ControlLabel>Subnetwork</ControlLabel>
                                        <FormControl
                                            componentClass="select"
                                            placeholder="select"
                                            onChange={(e) => this.setState({subnetworkID: e.target.value})}>
                                            {this.state.subnetworks.map(item =>
                                                <option key={item.id} value={item.id}>{item.name}</option>
                                            )}
                                        </FormControl>
                                    </FormGroup>

                                    <FormGroup controlId="formControlsFile">
                                        <ControlLabel>Configuration file</ControlLabel>
                                        <FormControl
                                            type="file"
                                            onChange={(e) => this.setState({configFile: e.target.files[0]})}/>
                                    </FormGroup>

                                    <FormGroup controlId="formControlsFile">
                                        <ControlLabel>Communication plugin</ControlLabel>
                                        <FormControl
                                            type="file"
                                            onChange={(e) => this.setState({commPluginFile: e.target.files[0]})}/>
                                    </FormGroup>

                                    <FormGroup controlId="formControlsFile">
                                        <ControlLabel>Data persistence plugin</ControlLabel>
                                        <FormControl
                                            type="file"
                                            onChange={(e) => this.setState({dataPersistencePluginFile: e.target.files[0]})}/>
                                    </FormGroup>

                                </form>
                            </div>
                    }

                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={() => this.dismiss()}>Cancel</Button>
                    <Button disabled={this.state.loading || !isFormValid} bsStyle="primary"
                            onClick={() => this.submit()}>Add</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default AddDetectorModal;