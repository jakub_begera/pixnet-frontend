import React, {Component} from 'react';
import {Alert, Button, ButtonToolbar, Col, Grid, Label, PageHeader, Row, Tab, Table, Tabs} from "react-bootstrap";
import {buildHandlerUrl} from "../../../utils/api";
import Loader from "../../../components/Loader";
import styled from "styled-components";
import {getConnectionStatusLabel, getMeasurementStatusLabel} from "../../../utils/viewStateUtils";
import AlertsHolder from "../../../components/AlertsHolder";
import BindToHandlerModal from "./BindToHandlerModal";
import ValueCommand from "./ValueCommand";
import ExecutionCommand from "./ExecutionCommand";
import UploadFileModal from "./UploadFileModal";

const ErrorWrapper = styled.div`
  width: 100%;
  padding: 1em;
`;

const ButtonWidth100 = styled(Button)`
  width: 100%;
  margin-bottom: .5em;
`;

class DetectorDetail extends Component {


    detectorID = this.props.match.params.detectorId;

    state = {
        alertsHolder: React.createRef(),
        detector: null,
        isLoading: false,
        error: null,
        isBindToHandlerModalVisible: false,
        isUploadFileModalVisible: false,
    };

    async fetch(showLoading = true) {
        if (showLoading) this.setState({isLoading: true});
        try {
            const response = await fetch(buildHandlerUrl("detector/getDetailById", {
                id: this.detectorID
            }));
            let json = await response.json();

            if (json) {
                if (json.success) {
                    this.setState({
                        detector: json.data,
                        error: null
                    });
                } else {
                    this.setState({
                        detector: null,
                        error: "Request fail (" + response.status + "): " + json.message
                    });
                }
            } else {
                this.setState({
                    detector: null,
                    error: "Request fail (" + response.status + ")"
                });
            }

        } catch (e) {
            this.setState({
                detector: null,
                error: e.message
            });
        }
        if (showLoading) this.setState({isLoading: false});
    }

    async removeDetector() {
        console.log("Removing detector with ID: " + this.detectorID);

        this.setState({
            isLoading: true
        });

        try {
            const response = await fetch(buildHandlerUrl("detector?id=" + this.detectorID), {
                method: 'DELETE'
            });
            let json = await response.json();

            if (json && json.success === false) {
                this.state.alertsHolder.current
                    .addAlert(`Removal of detector with ID ${this.detectorID} was unsuccessful. (${json.message})`, "danger")
            }
            this.goToDetectors()
        } catch (e) {
            console.error("Removing detector with ID " + this.detectorID + " was unsuccessful");
            this.state.alertsHolder.current
                .addAlert(`Removal of detector with ID ${this.detectorID} was unsuccessful. (${e.message})`, "danger")
        }
        this.setState({
            isLoading: false
        });
    }

    async connectDetector() {
        console.log("Connecting to detector with ID: " + this.detectorID);

        this.setState({
            isLoading: true
        });

        try {
            const response = await fetch(buildHandlerUrl("detector/connect?id=" + this.detectorID), {
                method: 'POST'
            });
            let json = await response.json();

            if (json && json.success === false) {
                this.state.alertsHolder.current
                    .addAlert(`Connecting to detector with ID ${this.detectorID} was unsuccessful. (${json.message})`, "danger")
            }
            await this.fetch(false)
        } catch (e) {
            this.state.alertsHolder.current
                .addAlert(`Connecting to detector with ID ${this.detectorID} was unsuccessful. (${e.message})`, "danger")
        }
        this.setState({
            isLoading: false
        });
    }

    async disconnectDetector() {
        console.log("Disconnecting from detector with ID: " + this.detectorID);

        this.setState({
            isLoading: true
        });

        try {
            const response = await fetch(buildHandlerUrl("detector/disconnect?id=" + this.detectorID), {
                method: 'POST'
            });
            let json = await response.json();

            if (json && json.success === false) {
                this.state.alertsHolder.current
                    .addAlert(`Disconnecting from detector with ID ${this.detectorID} was unsuccessful. (${json.message})`, "danger")
            }
            await this.fetch(false)
        } catch (e) {
            this.state.alertsHolder.current
                .addAlert(`Disconnecting from detector with ID ${this.detectorID} was unsuccessful. (${e.message})`, "danger")
        }
        this.setState({
            isLoading: false
        });
    }

    async unbindFromHandler() {
        this.setState({
            isLoading: true
        });

        console.log("Unbinding from detector with ID: " + this.detectorID);


        try {
            const response = await fetch(buildHandlerUrl("detector/unbindFromHandler", {
                detector_id: this.detectorID,
            }), {
                method: 'POST',

            });
            let json = await response.json();

            if (json && json.success !== true) {
                this.state.alertsHolder.current
                    .addAlert(`Detector (#${this.detectorID}) unbinding from handler was unsuccessful. (${json.message})`, "danger")
            }
            await this.fetch(false)
        } catch (e) {
            this.props.onError(`Detector (#${this.detectorID}) unbinding from handler was unsuccessful. (${e.message}) (${e.message})`);
        }

        this.setState({
            isLoading: false
        });
    }

    async componentDidMount() {
        await this.fetch();
    }

    goToDetectors() {
        this.props.history.push(`/detectors`)
    }

    renderPageHeader = () => {

        return <Grid>
            <ButtonToolbar>
                <Button bsStyle="primary" onClick={() => this.goToDetectors()}>Back</Button>
                <Button onClick={() => this.fetch()}>reload</Button>
            </ButtonToolbar>

            <br/>

            <Row>
                <AlertsHolder ref={this.state.alertsHolder}/>
            </Row>
        </Grid>
    };

    renderDetectorValueCommands = () => {
        const {detector} = this.state;

        if (!detector.comm || !detector.comm.supportedValueCommands)
            return <Alert bsStyle="warning">
                No value commands found. Is detector bound to any handler?
            </Alert>;

        return (
            <Row>
                {detector.comm.supportedValueCommands.map(item =>
                    <ValueCommand
                        key={item.id}
                        command={item}
                        detectorID={detector.id}
                    />
                )}
            </Row>
        );
    };

    renderDetectorExecutionCommands = () => {
        const {detector} = this.state;

        if (!detector.comm || !detector.comm.supportedExecutionCommands)
            return <Alert bsStyle="warning">
                No execution commands found. Is detector bound to any handler?
            </Alert>;

        return (
            <Row>
                {detector.comm.supportedExecutionCommands.map(item =>
                    <ExecutionCommand
                        key={item.id}
                        command={item}
                        detectorID={detector.id}
                    />
                )}
            </Row>
        );
    };

    renderDetector = () => {
        const {isLoading, error, detector} = this.state;
        const isFileUploadAllowed = detector && detector.comm && detector.comm.connected
            && detector.comm.acceptedFilesKeys && detector.comm.acceptedFilesKeys.length > 0;
        if (isLoading)
            return <Loader/>;

        if (error || !detector)
            return <ErrorWrapper>
                <Alert bsStyle="danger">
                    {error
                        ? <span><strong>Detector loading error: </strong>{error}</span>
                        : <strong>Detector loading general error</strong>
                    }
                </Alert>
            </ErrorWrapper>;

        const overview = <Col lg={5} md={4} sm={12}>
            <h3>Overview</h3>

            <Table hover responsive>
                <tbody>
                <tr>
                    <td><strong>ID:</strong></td>
                    <td>{detector.id}</td>
                </tr>
                <tr>
                    <td><strong>Name:</strong></td>
                    <td>{detector.name}</td>
                </tr>
                <tr>
                    <td><strong>Connection status:</strong></td>
                    <td>
                        <Label
                            bsStyle={getConnectionStatusLabel(
                                detector.status && detector.status.connection
                                    ? detector.status.connection
                                    : "UNKNOWN"
                            )}>
                            {
                                detector.status && detector.status.connection
                                    ? detector.status.connection
                                    : "UNKNOWN"
                            }
                        </Label>
                    </td>
                </tr>
                <tr>
                    <td><strong>Measurement status:</strong></td>
                    <td>
                        <Label
                            bsStyle={getMeasurementStatusLabel(
                                detector.status && detector.status.measurement
                                    ? detector.status.measurement
                                    : "UNKNOWN"
                            )}>
                            {
                                detector.status && detector.status.measurement
                                    ? detector.status.measurement
                                    : "UNKNOWN"
                            }
                        </Label>
                    </td>
                </tr>
                <tr>
                    <td><strong>Subnetwork:</strong></td>
                    <td>{`${detector.subnetwork.name} (#${detector.subnetwork.id})`}</td>
                </tr>
                <tr>
                    <td><strong>Bound handler:</strong></td>
                    <td>{detector.handler
                        ? `${detector.handler.name} (#${detector.handler.id})`
                        : `No handler`
                    }</td>
                </tr>
                </tbody>
            </Table>
        </Col>;

        const comm = <Col lg={5} md={5} sm={12}>
            <h3>Communication plugin</h3>
            {detector.comm
                ? <Table hover responsive>
                    <tbody>
                    <tr>
                        <td><strong>Detector type:</strong></td>
                        <td>{detector.comm.detectorType}</td>
                    </tr>
                    <tr>
                        <td><strong>Readout name:</strong></td>
                        <td>{detector.comm.readoutName}</td>
                    </tr>
                    <tr>
                        <td><strong>Number of sensors:</strong></td>
                        <td>{detector.comm.sensorsCount}</td>
                    </tr>
                    <tr>
                        <td><strong>Width:</strong></td>
                        <td>{detector.comm.detectorWidth}</td>
                    </tr>
                    <tr>
                        <td><strong>Height:</strong></td>
                        <td>{detector.comm.detectorHeight}</td>
                    </tr>
                    <tr>
                        <td><strong>Number frames waiting in queue:</strong></td>
                        <td>{detector.comm.dataFrameQueueSize}</td>
                    </tr>
                    <tr>
                        <td><strong>Detector connection:</strong></td>
                        <td>{detector.comm.connected ? "connected" : "disconnected"}</td>
                    </tr>
                    </tbody>
                </Table>
                : <Alert bsStyle="warning">
                    <strong>Warning: </strong>Communication plugin not initialized.
                </Alert>
            }
        </Col>;

        const action = <Col lg={2} md={3} sm={4}>
            <h3>Action</h3>

            <Grid fluid>
                {detector.comm && !detector.comm.connected &&
                <Row>
                    <ButtonWidth100
                        bsStyle="primary"
                        onClick={() => this.connectDetector()}
                    >
                        Connect
                    </ButtonWidth100>
                </Row>
                }

                {detector.comm && detector.comm.connected &&
                <Row>
                    <ButtonWidth100
                        bsStyle="warning"
                        onClick={() => this.disconnectDetector()}
                    >
                        Disconnect
                    </ButtonWidth100>
                </Row>
                }

                <Row>
                    <ButtonWidth100
                        bsStyle="primary"
                        onClick={() => this.setState({isBindToHandlerModalVisible: true})}
                    >
                        Bind to handler
                    </ButtonWidth100>
                </Row>

                {detector.handler &&
                <Row>
                    <ButtonWidth100
                        bsStyle="warning"
                        onClick={() => this.unbindFromHandler()}
                    >
                        Unbind from handler
                    </ButtonWidth100>
                </Row>
                }

                {isFileUploadAllowed &&
                <Row>
                    <ButtonWidth100
                        bsStyle="primary"
                        onClick={() => this.setState({isUploadFileModalVisible: true})}
                    >
                        Upload file
                    </ButtonWidth100>
                </Row>
                }

                <Row>
                    <ButtonWidth100
                        bsStyle="danger"
                        onClick={() => this.removeDetector()}
                    >
                        Remove
                    </ButtonWidth100>
                </Row>
            </Grid>

        </Col>;

        return <Grid fluid>
            <PageHeader>
                {detector.name}&nbsp;
                <small>#{detector.id}</small>
            </PageHeader>

            <Row>
                {overview}
                {comm}
                {action}
            </Row>

            <br/>

            {/* content */}
            <Row>
                <Tabs defaultActiveKey={1} id="detector-content-tabs">
                    <Tab eventKey={1} title="Value commands">
                        <br/>
                        {this.renderDetectorValueCommands()}
                    </Tab>
                    <Tab eventKey={2} title="Execution commands">
                        <br/>
                        {this.renderDetectorExecutionCommands()}
                    </Tab>
                </Tabs>
            </Row>


            <BindToHandlerModal
                show={this.state.isBindToHandlerModalVisible}
                onHide={() => this.setState({isBindToHandlerModalVisible: false})}
                onSuccess={() => {
                    this.setState({isBindToHandlerModalVisible: false});
                    this.fetch();
                }}
                onError={(message) => {
                    this.setState({isBindToHandlerModalVisible: false});
                    this.state.alertsHolder.current.addAlert(message, "danger")
                }}
                detector={this.state.detector}
            />

            {isFileUploadAllowed &&
            <UploadFileModal
                show={this.state.isUploadFileModalVisible}
                onHide={() => this.setState({isUploadFileModalVisible: false})}
                onSuccess={(message) => {
                    this.setState({isUploadFileModalVisible: false});
                    this.state.alertsHolder.current.addAlert(message, "success")
                }}
                onError={(message) => {
                    this.setState({isUploadFileModalVisible: false});
                    this.state.alertsHolder.current.addAlert(message, "danger")
                }}
                detector={this.state.detector}
            />
            }

        </Grid>;

    };

    render() {

        return (
            <Grid>

                {this.renderPageHeader()}

                {this.renderDetector()}

            </Grid>
        );
    }

}

export default DetectorDetail;
