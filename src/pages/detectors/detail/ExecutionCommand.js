import React, {Component} from "react";
import {Button, Col, Grid, Label, Panel, Row, Table} from "react-bootstrap";
import Loader from "../../../components/Loader";
import styled from "styled-components";
import ExecutionCommandInput from "./ExecutionCommandInput";
import {buildHandlerUrl} from "../../../utils/api";
import {formatValueUnit} from "../../../utils/dataUtils";

const ExecuteButton = styled(Button)`
  width: 80%;
  margin-top: .5em;
  margin-bottom: .5em;
`;

class ExecutionCommand extends Component {

    state = {
        isLoading: false,
        panelStyle: "default",
        values: new Map(),
        result: null,
        error: null,
        executionSuccess: null
    };

    getValueFromValuePayload(payload) {
        if (!payload || !payload.type) return "[no data]";
        switch (payload.type) {
            case "INT":
                return payload.intValue;
            case "LONG":
                return payload.longValue;
            case "FLOAT":
                return payload.floatValue;
            case "DOUBLE":
                return payload.doubleValue;
            case "BOOLEAN":
                return payload.booleanValue;
            case "STRING":
                return payload.stringValue;
            default:
                return `[Unrecognized payload type ${payload.type}]`
        }
    }

    isInputValid() {
        let inputValuesModel = this.props.command.inputValuesModel;
        if (!inputValuesModel || inputValuesModel.length === 0) {
            return true;
        }

        for (let i = 0; i < inputValuesModel.length; i++) {
            let valueID = inputValuesModel[i].valueID;
            if (!this.state.values.has(valueID) || !this.state.values.get(valueID)) return false;
        }

        return true;
    }

    buildInput() {
        let input = {};

        let inputValuesModel = this.props.command.inputValuesModel;
        if (!inputValuesModel) return input;

        inputValuesModel.forEach(item => {

            switch (item.valueModel["@type"]) {
                case "IntValueModel":
                    input[item.valueID] = {intValue: this.state.values.get(item.valueID)};
                    break;
                case "LongValueModel":
                    input[item.valueID] = {longValue: this.state.values.get(item.valueID)};
                    break;
                case "FloatValueModel":
                    input[item.valueID] = {floatValue: this.state.values.get(item.valueID)};
                    break;
                case "DoubleValueModel":
                    input[item.valueID] = {doubleValue: this.state.values.get(item.valueID)};
                    break;
                case "StringValueModel":
                    input[item.valueID] = {stringValue: this.state.values.get(item.valueID)};
                    break;
                case "BooleanValueModel":
                    input[item.valueID] = {booleanValue: this.state.values.get(item.valueID)};
                    break;
            }
        });

        return input;
    }

    async execute() {
        this.setState({
            isLoading: true,
            panelStyle: "warning",
        });
        const {command, detectorID} = this.props;

        try {
            const response = await fetch(buildHandlerUrl("detector/executeExecutionCommand"), {
                method: 'POST',
                body: JSON.stringify({
                    detectorID: detectorID,
                    commandID: command.id,
                    input: this.buildInput()
                }),
                headers: {
                    "Content-Type": "application/json; charset=utf-8"
                }

            });
            let json = await response.json();

            if (json && json.success !== true) {
                this.setState({
                    error: `Error: ${json.message}`,
                    result: null,
                    panelStyle: "danger",
                    executionSuccess: false,
                })
            } else {
                this.setState({
                    error: null,
                    result: json.data,
                    panelStyle: "success",
                    executionSuccess: true,
                })
            }
        } catch (e) {
            this.setState({
                error: `Error: ${e.message}`,
                result: null,
                panelStyle: "danger",
                executionSuccess: false,
            })
        }

        this.setState({
            isLoading: false
        });

    }

    renderPanelContentExecutionCommand = () => {
        const {command} = this.props;

        return (
            <Grid>
                <Row>
                    <Col lg={10} md={9} xs={12}>
                        {(!command.inputValuesModel || command.inputValuesModel.length === 0)
                            ? <span>No input</span>
                            : command.inputValuesModel.map(item =>
                                <ExecutionCommandInput
                                    key={item.valueID}
                                    model={item}
                                    onValueChosen={(value) => {
                                        let values = this.state.values;
                                        values.set(item.valueID, value);
                                        this.setState({
                                            values: values
                                        })
                                    }}
                                    value={this.state.values.get(item.valueID)}
                                />
                            )
                        }
                    </Col>
                    <Col lg={2} md={3} xs={8}>
                        <ExecuteButton
                            bsStyle="primary"
                            disabled={!this.isInputValid()}
                            onClick={() => this.execute()}
                        >
                            Execute
                        </ExecuteButton>
                    </Col>
                </Row>
                <Row>{this.state.message}</Row>
            </Grid>
        );
    };

    renderPanelContentExecutionCommandGroup = () => {
        const {command} = this.props;

        if (!command.executionCommands || command.executionCommands.length === 0)
            return <span>No commands found</span>;

        return <Grid>
            {command.executionCommands.map(item =>
                <ExecutionCommand
                    key={item.id}
                    command={item}
                    detectorID={this.props.detectorID}
                />
            )}
        </Grid>;
    };

    renderPanelContent = () => {
        const {command} = this.props;
        let loading = this.state.isLoading;
        if (loading) return <Loader small/>;
        if (command["@type"] === "ExecutionCommand") return this.renderPanelContentExecutionCommand();
        if (command["@type"] === "ExecutionCommandGroup") return this.renderPanelContentExecutionCommandGroup();

        return <span>Unrecognized type ({command["@type"]})</span>;
    };

    renderResult = () => {
        const {error, result, executionSuccess} = this.state;
        const {command} = this.props;
        let model = command.outputValuesModel;

        if (executionSuccess === null) return null; // not executed yet

        let content = null;
        if (executionSuccess) {
            if (result && model) {
                content = <Table hover responsive>
                    <tbody>{model.map(item => {
                        const valueUnit = formatValueUnit(item.valueUnit);
                        let payload = result[item.valueID];
                        let name = item.valueName;
                        let value = this.getValueFromValuePayload(payload);

                        return <tr>
                            <td><strong>{name}</strong></td>
                            <td>{value}{valueUnit && ` ${valueUnit}`}</td>
                        </tr>
                    })
                    }</tbody>
                </Table>;


            }
        }

        return <Panel.Footer>
            <Grid>
                <Col md={4} xs={12}>
                    <Row>
                        <strong>Status: </strong>
                        <Label bsStyle={this.state.panelStyle}>{executionSuccess ? "success" : "error"}</Label>
                    </Row>
                    <br/>
                    {error &&
                    <Row>
                        <strong>Error: </strong>{error}
                    </Row>
                    }
                </Col>
                <Col md={4} xs={12}>
                    {content
                        ? content
                        : "Empty response"
                    }
                </Col>
            </Grid>
        </Panel.Footer>;
    };


    render() {
        const {command} = this.props;
        const {isLoading, panelStyle} = this.state;


        return (
            <Panel
                defaultExpanded={true}
                bsStyle={panelStyle}
            >
                <Panel.Heading toggle>
                    <Panel.Title toggle>
                        {command.name}
                    </Panel.Title>
                </Panel.Heading>
                <Panel.Collapse>
                    <Panel.Body>{this.renderPanelContent()}</Panel.Body>
                    {!isLoading && this.renderResult()}
                </Panel.Collapse>
            </Panel>
        )
    }

}

export default ExecutionCommand;