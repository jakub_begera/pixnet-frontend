import React, {Component} from "react";
import {
    Button,
    ButtonToolbar,
    Col,
    ControlLabel,
    Form,
    FormControl,
    FormGroup,
    Grid,
    InputGroup,
    Panel,
    Row
} from "react-bootstrap";
import Loader from "../../../components/Loader";
import {buildHandlerUrl} from "../../../utils/api";
import {formatValueUnit} from "../../../utils/dataUtils";

class ValueCommand extends Component {

    state = {
        isLoading: false,
        value: null,
        message: null,
        panelStyle: "default"
    };

    command = this.props.command;

    isValueCommandType() {
        return this.props.command["@type"] === "ValueCommand"
    }

    isSetter() {
        return this.command.accessType === "SETTER" || this.command.accessType === "SETTER_AND_GETTER"
    }

    isGetter() {
        return this.command.accessType === "GETTER" || this.command.accessType === "SETTER_AND_GETTER"
    }

    isDiscrete() {
        return this.command.inputValueModel && this.command.inputValueModel.discrete != null;
    }

    setNewValue(value) {

        if (this.isDiscrete()) {
            if (value == null || value === "null") {
                this.setState({
                    value: null,
                    message: `${this.command.name} is not set.`,
                    panelStyle: "warning"
                })
            } else {
                this.setState({
                    value: value,
                    message: null,
                    panelStyle: "default"
                })
            }
        } else {
            let model = this.command.inputValueModel;
            switch (model["@type"]) {
                case "IntValueModel":
                case "LongValueModel":
                case "FloatValueModel":
                case "DoubleValueModel":
                    let number = Number(value);
                    if (Number.isNaN(number) || number > model.max || number < model.min) {
                        this.setState({
                            value: null,
                            message: `Value must be in: ` +
                                `<${this.command.inputValueModel.min}, ${this.command.inputValueModel.max}>`,
                            panelStyle: "warning"
                        });
                    } else {
                        this.setState({
                            value: value,
                            message: null,
                            panelStyle: "default"
                        });
                    }
                    break;
                case "StringValueModel":
                    this.setState({
                        value: value,
                        message: null,
                        panelStyle: "default"
                    });
                    break;
            }
        }

    }

    async executeGet() {
        this.setState({
            isLoading: true,
            panelStyle: "warning",
        });

        const {command, detectorID} = this.props;

        try {
            const response = await fetch(buildHandlerUrl("detector/executeValueCommand"), {
                method: 'POST',
                body: JSON.stringify({
                    detectorID: detectorID,
                    commandID: command.id,
                    operation: "GET"
                }),
                headers: {
                    "Content-Type": "application/json; charset=utf-8"
                }

            });
            let json = await response.json();

            if (json && json.success !== true || !json.data) {
                this.setState({
                    message: `Error: ${json.message}`,
                    messageTye: "danger",
                    panelStyle: "danger",
                    isLoading: false
                });
                return
            }

            let value = null;
            if (json.data) {

                switch (json.data.type) {
                    case "STRING":
                        value = json.data.stringValue;
                        break;
                    case "INT":
                        value = json.data.intValue;
                        break;
                    case "LONG":
                        value = json.data.longValue;
                        break;
                    case "FLOAT":
                        value = json.data.floatValue;
                        break;
                    case "DOUBLE":
                        value = json.data.doubleValue;
                        break;
                    case "BOOLEAN":
                        value = json.data.booleanValue ? "TRUE" : "FALSE";
                        break;
                }
            }

            this.setState({
                message: `Success`,
                messageTye: "success",
                value: value,
                panelStyle: "success",
            })
        } catch (e) {
            this.setState({
                message: `Error: ${e.message}`,
                messageTye: "danger",
                panelStyle: "danger",
            })
        }

        this.setState({
            isLoading: false
        });
    }

    async executeSet() {
        this.setState({
            isLoading: true,
            panelStyle: "warning",
        });

        const {command, detectorID} = this.props;

        try {
            let payload = null;
            switch (command.inputValueModel["@type"]) {
                case "IntValueModel":
                    payload = {intValue: this.state.value};
                    break;
                case "LongValueModel":
                    payload = {longValue: this.state.value};
                    break;
                case "FloatValueModel":
                    payload = {floatValue: this.state.value};
                    break;
                case "DoubleValueModel":
                    payload = {doubleValue: this.state.value};
                    break;
                case "StringValueModel":
                    payload = {stringValue: this.state.value};
                    break;
            }
            const response = await fetch(buildHandlerUrl("detector/executeValueCommand"), {
                method: 'POST',
                body: JSON.stringify({
                    detectorID: detectorID,
                    commandID: command.id,
                    operation: "SET",
                    payload: payload
                }),
                headers: {
                    "Content-Type": "application/json; charset=utf-8"
                }

            });
            let json = await response.json();

            if (json && json.success !== true) {
                this.setState({
                    message: `Error: ${json.message}`,
                    messageTye: "danger",
                    panelStyle: "danger",
                })
            } else {
                this.setState({
                    message: `Success`,
                    messageTye: "success",
                    panelStyle: "success",
                })
            }

        } catch (e) {
            this.setState({
                message: `Error: ${e.message}`,
                messageTye: "danger",
                panelStyle: "danger",
            })
        }

        this.setState({
            isLoading: false
        });
    }

    async componentDidMount() {
    }

    renderNonDiscreteInput = () => {
        const content = <FormControl
            readOnly={!this.isSetter()}
            type="text"
            placeholder={this.isSetter() ? "Enter new value" : "Click on get"}
            value={this.state.value}
            onChange={(e) => this.setNewValue(e.target.value)}
        />;

        const valueUnit = formatValueUnit(this.command.valueUnit);
        if (valueUnit)
            return (
                <InputGroup>
                    {content}
                    <InputGroup.Addon>{valueUnit}</InputGroup.Addon>
                </InputGroup>
            );
        else
            return content;
    };

    renderValueForm = () => {
        const command = this.props.command;

        return <Grid fluid={true}>
            <Row>
                <Col sm={6} fluid={true}>
                    <Form componentClass="fieldset" horizontal>
                        <FormGroup>
                            <Col componentClass={ControlLabel} xs={4}>
                                Value:
                            </Col>
                            <Col xs={8}>
                                {this.isDiscrete() && this.isSetter()
                                    ? <FormControl
                                        componentClass="select"
                                        onChange={(e) => this.setNewValue(e.target.value)}
                                    >
                                        <option value={"null"}>Select</option>
                                        {
                                            Object.keys(command.inputValueModel.discrete).map(key =>
                                                <option
                                                    key={command.inputValueModel.discrete[key]}
                                                    value={command.inputValueModel.discrete[key]}>
                                                    {key}
                                                </option>
                                            )
                                        }
                                    </FormControl>
                                    : this.renderNonDiscreteInput()
                                }
                                <FormControl.Feedback/>
                            </Col>
                        </FormGroup>


                    </Form>
                </Col>
                <Col sm={6}>
                    <ButtonToolbar>
                        {this.isGetter() &&
                        <Button
                            bsStyle="primary"
                            bsSize="small"
                            onClick={() => this.executeGet()}
                        >GET</Button>
                        }
                        {this.isSetter() &&
                        <Button
                            bsStyle="primary"
                            bsSize="small"
                            disabled={this.state.value === null}
                            onClick={() => this.executeSet()}
                        >SET</Button>
                        }
                    </ButtonToolbar>
                </Col>

            </Row>

        </Grid>
    };

    render() {
        const command = this.props.command;

        let content = null;
        if (this.state.isLoading) {
            content = <Loader small/>
        } else if (command["@type"] === "ValueCommand") {
            content = this.renderValueForm()
        } else if (command["@type"] === "ValueCommandGroup") {
            content = command.valueCommands.map(item =>
                <ValueCommand
                    key={item.id}
                    command={item}
                    detectorID={this.props.detectorID}
                />
            );
        } else {
            content = <span>Unrecognized type ({command["@type"]})</span>
        }

        return (
            <Panel
                defaultExpanded={this.isValueCommandType()}
                bsStyle={this.state.panelStyle}
            >
                <Panel.Heading toggle>
                    <Panel.Title toggle>
                        {command.name}
                    </Panel.Title>
                </Panel.Heading>
                <Panel.Collapse>
                    <Panel.Body>{content}</Panel.Body>
                    {this.state.message &&
                    <Panel.Footer>{this.state.message}</Panel.Footer>
                    }
                </Panel.Collapse>
            </Panel>
        )
    }
}

export default ValueCommand;