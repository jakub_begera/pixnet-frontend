import React, {Component} from "react";
import {Col, ControlLabel, Form, FormControl, FormGroup, HelpBlock, InputGroup, Row} from "react-bootstrap";
import {formatValueUnit} from "../../../utils/dataUtils";

class ExecutionCommandInput extends Component {

    state = {
        value: this.props.value,
        message: null,
        panelStyle: null
    };

    model = this.props.model;

    isDiscrete() {
        return this.model.valueModel.discrete != null && this.model.valueModel.discrete.length !== 0;
    }

    setNewValue(value) {
        var newVal = null;
        if (this.isDiscrete()) {
            if (value == null || value === "null") {
                newVal = null;
                this.setState({
                    value: null,
                    message: `${this.model.valueName} is not set.`,
                    panelStyle: "warning"
                })
            } else {
                newVal = value;
                this.setState({
                    value: value,
                    message: null,
                    panelStyle: "success"
                })
            }
        } else {
            let model = this.model.valueModel;
            switch (model["@type"]) {
                case "IntValueModel":
                case "LongValueModel":
                case "FloatValueModel":
                case "DoubleValueModel":
                    let number = Number(value);
                    if (value === "" || Number.isNaN(number) || number > model.max || number < model.min) {
                        newVal = null;
                        this.setState({
                            value: null,
                            message: `Value must be in: ` +
                                `<${model.min}, ${model.max}>`,
                            panelStyle: "warning"
                        });
                    } else {
                        newVal = value;
                        this.setState({
                            value: value,
                            message: null,
                            panelStyle: "success"
                        });
                    }
                    break;
                case "StringValueModel":
                    newVal = value;
                    this.setState({
                        value: value,
                        message: null,
                        panelStyle: "success"
                    });
                    break;
                default:
                    newVal = value;
                    this.setState({
                        value: value,
                        message: null,
                        panelStyle: "success"
                    });
                    break;
            }
        }

        // propagate value up
        this.props.onValueChosen(newVal);
    }


    renderFormControl = () => {

        if (this.isDiscrete()) {
            return (
                <FormControl
                    componentClass="select"
                    onChange={(e) => this.setNewValue(e.target.value)}
                    value={this.state.value}
                >
                    <option value={"null"}>Select</option>
                    {
                        Object.keys(this.model.valueModel.discrete).map(key =>
                            <option
                                key={this.model.valueModel.discrete[key]}
                                value={this.model.valueModel.discrete[key]}>
                                {key}
                            </option>
                        )
                    }
                </FormControl>
            )
        } else {
            const content = <FormControl
                type="text"
                placeholder="enter value"
                onChange={(e) => this.setNewValue(e.target.value)}
                value={this.state.value}
            />;
            const valueUnit = formatValueUnit(this.model.valueUnit);
            if (valueUnit)
                return (
                    <InputGroup>
                        {content}
                        <InputGroup.Addon>{valueUnit}</InputGroup.Addon>
                    </InputGroup>
                );
            else
                return content;
        }
    };

    render() {

        let model = this.props.model;

        return (
            <Row>
                <Form horizontal>
                    <FormGroup validationState={this.state.panelStyle}>
                        <Col componentClass={ControlLabel} sm={3}>
                            {model.valueName}
                        </Col>
                        <Col sm={8}>
                            {this.renderFormControl()}
                            {this.state.message &&
                            <HelpBlock>{this.state.message}</HelpBlock>
                            }
                        </Col>
                    </FormGroup>
                </Form>
            </Row>
        );
    }
}

export default ExecutionCommandInput;