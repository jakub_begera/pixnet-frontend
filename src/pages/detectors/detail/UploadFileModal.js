import React, {Component} from 'react';
import {Button, ControlLabel, FormControl, FormGroup, Modal} from "react-bootstrap";
import Loader from "../../../components/Loader";
import {buildHandlerUrl} from "../../../utils/api";

class UploadFileModal extends Component {

    state = {
        fileID: this.props.detector.comm.acceptedFilesKeys[0],
        loading: false,
        file: null
    };

    isFormValid() {
        return this.state.fileID && this.state.file
    }

    async submit() {
        this.setState({loading: true});

        try {

            const url = buildHandlerUrl("detector/uploadFile", {
                detectorID: this.props.detector.id,
                fileID: this.state.fileID
            });

            const formData = new FormData();
            formData.append('file', this.state.file);

            const options = {
                method: 'POST',
                body: formData,
            };

            let response = await fetch(url, options);
            let json = await response.json();

            if (json && json.success !== true) {
                this.props.onError(`File uploading was unsuccessful. (${json.message})`);
            } else {
                this.props.onSuccess(`File with id ${this.state.fileID} was successfully uploaded.`);
            }
        } catch (e) {
            this.props.onError(`File uploading was unsuccessful. (${e.message})`);
        }

        this.setState({loading: false});
    }

    dismiss() {
        this.setState({loading: false});
        this.props.onHide()
    }

    render() {

        const isFormValid = this.isFormValid();

        return (
            <Modal
                {...this.props}
                bsSize="large"
                onHide={() => this.dismiss()}
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">Upload file</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    {
                        this.state.loading
                            ? <Loader/>
                            : <form>

                                <FormGroup controlId="formControlsSelect">
                                    <ControlLabel>Choose handler</ControlLabel>
                                    <FormControl
                                        componentClass="select"
                                        placeholder="select"
                                        onChange={(e) => this.setState({fileID: e.target.value})}>
                                        {this.props.detector.comm.acceptedFilesKeys.map(item =>
                                            <option
                                                key={item}
                                                value={item}
                                            >
                                                {item}
                                            </option>
                                        )}
                                    </FormControl>
                                </FormGroup>

                                <FormGroup controlId="formControlsFile">
                                    <ControlLabel>File</ControlLabel>
                                    <FormControl
                                        type="file"
                                        onChange={(e) => this.setState({file: e.target.files[0]})}/>
                                </FormGroup>

                            </form>
                    }

                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={() => this.dismiss()}>Cancel</Button>
                    <Button disabled={this.state.loading || !isFormValid} bsStyle="primary"
                            onClick={() => this.submit()}>Upload</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default UploadFileModal;