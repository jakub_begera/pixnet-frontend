import React, {Component} from 'react';
import {Button, ControlLabel, FormControl, FormGroup, Modal} from "react-bootstrap";
import Loader from "../../../components/Loader";
import {buildHandlerUrl} from "../../../utils/api";

class BindToHandlerModal extends Component {

    state = {
        handlerID: -1,
        loading: false,
        handlers: []
    };

    isFormValid() {
        return this.state.handlerID !== -1
    }

    async fetchHandlers() {
        this.setState({loading: true});

        let error = null;
        try {
            const response = await fetch(buildHandlerUrl("handler/getAll"));
            let json = await response.json();

            if (json) {
                if (json.success) {
                    if (json.data && json.data.length > 0) {
                        this.setState({
                            handlers: json.data,
                            handlerID: json.data[0].id
                        });
                    } else {
                        error = "No available handlers were found. Did you add some?";
                    }

                } else {
                    error = "Request fail (" + response.status + "): " + json.message;
                }
            } else {
                error = "Request fail (" + response.status + ")";
            }

        } catch (e) {
            error = e.message;
        }

        if (error) {
            this.props.onError(`Binding of detector was unsuccessful. Unable to fetch handlers: ${error}.`);
        }

        this.setState({loading: false});
    }

    async submit() {
        this.setState({loading: true});

        const detectorID = this.props.detector.id;

        try {
            const response = await fetch(buildHandlerUrl("detector/bindToHandler", {
                detector_id: detectorID,
                handler_id: this.state.handlerID
            }), {
                method: 'POST',

            });
            let json = await response.json();

            if (json && json.success !== true) {
                this.props.onError(`Detector (#${detectorID}) binding to handler (#${this.state.handlerID}) was unsuccessful. (${json.message})`);
            } else {
                this.props.onSuccess();
            }
        } catch (e) {
            this.props.onError(`Detector (#${detectorID}) binding to handler (#${this.state.handlerID}) was unsuccessful. (${e.message})`);
        }

        this.setState({loading: false});
    }

    dismiss() {
        this.setState({loading: false});
        this.props.onHide()
    }

    async componentDidMount() {
        await this.fetchHandlers();
    }

    isHandlerPossible(handler) {
        let detector = this.props.detector;

        if (detector.subnetwork.id !== handler.subnetwork.id) return false;
        return !(detector.handler && detector.handler.id === handler.id);
    }

    render() {

        const isFormValid = this.isFormValid();

        return (
            <Modal
                {...this.props}
                bsSize="large"
                onHide={() => this.dismiss()}
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">Bind detector <small>#{this.props.detector.id}</small></Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    {
                        this.state.loading
                            ? <Loader/>
                            : <form>

                                <FormGroup controlId="formControlsSelect">
                                    <ControlLabel>Choose handler</ControlLabel>
                                    <FormControl
                                        componentClass="select"
                                        placeholder="select"
                                        onChange={(e) => this.setState({handlerID: e.target.value})}>
                                        {this.state.handlers.map(item =>
                                            <option
                                                key={item.id}
                                                value={item.id}
                                                disabled={!this.isHandlerPossible(item)}
                                            >
                                                {item.name} (#{item.id})
                                            </option>
                                        )}
                                    </FormControl>
                                </FormGroup>

                            </form>
                    }

                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={() => this.dismiss()}>Cancel</Button>
                    <Button disabled={this.state.loading || !isFormValid} bsStyle="primary"
                            onClick={() => this.submit()}>Bind</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default BindToHandlerModal;