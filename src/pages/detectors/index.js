import React, {Component} from 'react';
import {Button, ButtonToolbar, Grid, PageHeader} from "react-bootstrap";
import AlertsHolder from "../../components/AlertsHolder";
import DetectorsTable from "./DetectorsTable";
import AddDetectorModal from "./AddDetectorModal";

class Detectors extends Component {

    state = {
        alertsHolder: React.createRef(),
        detectorsTable: React.createRef(),
        isAddModalVisible: false
    };

    render() {
        return (
            <Grid>
                <PageHeader>
                    Detectors
                </PageHeader>

                <AlertsHolder ref={this.state.alertsHolder}/>

                <ButtonToolbar>

                    <Button bsStyle="primary"
                            onClick={() => this.setState({isAddModalVisible: true})}
                    >
                        Add
                    </Button>

                    <Button
                        onClick={() => this.state.detectorsTable.current.fetchItems()}
                    >
                        reload
                    </Button>

                </ButtonToolbar>

                <br/>

                <DetectorsTable
                    ref={this.state.detectorsTable}
                    addErrorAlert={(message) => this.state.alertsHolder.current.addAlert(message, "danger")}
                    addWarningAlert={(message) => this.state.alertsHolder.current.addAlert(message, "warning")}
                    history={this.props.history}
                />

                <AddDetectorModal
                    show={this.state.isAddModalVisible}
                    onHide={() => this.setState({isAddModalVisible: false})}
                    onSuccess={() => {
                        this.setState({isAddModalVisible: false});
                        this.state.detectorsTable.current.fetchItems();
                    }}
                    onError={(message) => {
                        this.setState({isAddModalVisible: false});
                        this.state.alertsHolder.current.addAlert(message, "danger")
                    }}
                />

            </Grid>
        );
    }
}

export default Detectors;
