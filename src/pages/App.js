import React, {Component} from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import './App.css';
import Detectors from "./detectors";
import Handlers from "./handlers";
import Subnetworks from "./subnetworks";
import {Route, Switch} from "react-router";
import AppNavbar from "../components/AppNavbar";
import LandingPage from "./landing";
import DetectorDetail from "./detectors/detail";

class App extends Component {
    render() {
        return (
            <Router>
                <div className="App">

                    <AppNavbar/>

                    <Switch>
                        <Route exact={true} path="/" component={LandingPage}/>
                        <Route exact path="/detectors" component={Detectors}/>
                        <Route path="/detectors/:detectorId" component={DetectorDetail}/>
                        <Route path="/handlers" component={Handlers}/>
                        <Route path="/subnetworks" component={Subnetworks}/>
                    </Switch>

                </div>

            </Router>
        );
    }
}

export default App;