import React, {Component} from 'react';
import {buildHandlerUrl} from "../../utils/api";
import Loader from "../../components/Loader";
import {Alert, Button, Table} from "react-bootstrap";
import {BeatLoader} from "react-spinners";
import {COLOR_ACCENT} from "../../utils/colors";
import {ErrorWrapper} from "../../utils/styleUtils";


class SubnetworksTable extends Component {

    state = {
        items: [],
        isLoading: false,
        error: null
    };

    async fetchItems(showLoading = true) {
        if (showLoading) this.setState({isLoading: true});
        try {
            const response = await fetch(buildHandlerUrl("subnetwork/getAll"));
            let json = await response.json();

            if (json) {
                if (json.success) {
                    this.setState({
                        items: json.data,
                        error: null
                    });
                } else {
                    this.setState({
                        items: [],
                        error: "Request fail (" + response.status + "): " + json.message
                    });
                }
            } else {
                this.setState({
                    items: [],
                    error: "Request fail (" + response.status + ")"
                });
            }

        } catch (e) {
            this.setState({
                items: [],
                error: e.message
            });
        }
        if (showLoading) this.setState({isLoading: false});
    }

    async removeSubnetwork(id) {
        console.log("Removing subnetwork with ID: " + id);

        const index = this.state.items.findIndex(item => item.id === id);
        let items = this.state.items;
        items[index].loading = true;

        this.setState({
            items: items
        });


        try {
            const response = await fetch(buildHandlerUrl("subnetwork/deleteById?id=" + id), {
                method: 'DELETE'
            });
            let json = await response.json();

            if (json && json.success === false) {
                this.props.onError(`Removal of subnetwork with ID ${id} was unsuccessful. (${json.message})`);
            }
            await this.fetchItems(false)
        } catch (e) {
            console.error("Removing subnetwork with ID " + id + " was unsuccessful");
            this.props.addErrorAlert(`Removal of subnetwork with ID ${id} was unsuccessful. (${e.message})`);
        }
    }

    async componentDidMount() {
        await this.fetchItems();
    }

    render() {
        const {isLoading, error, items} = this.state;

        if (isLoading) {
            return (<Loader/>)
        }


        if (error) {
            return (
                <ErrorWrapper>
                    <Alert bsStyle="danger"><strong>Subnetworks loading error: </strong>{error}</Alert>
                </ErrorWrapper>
            )
        }

        if (items.length === 0) {
            return (
                <ErrorWrapper>
                    <Alert bsStyle="info">No subnetworks found.</Alert>
                </ErrorWrapper>
            )
        }

        const rows = items.map(item =>
            <tr key={item.id}>
                <th>{item.id}</th>
                <td>{item.name}</td>
                <td>
                    {
                        item.loading
                            ? <BeatLoader size={8} color={COLOR_ACCENT}/>
                            : <Button bsStyle="danger" bsSize="xsmall"
                                      onClick={() => this.removeSubnetwork(item.id)}>
                                Remove {item.state}
                            </Button>
                    }
                </td>
            </tr>
        );

        return (
            <Table responsive>
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </Table>
        )
    }

}

export default SubnetworksTable;