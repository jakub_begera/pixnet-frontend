import React, {Component} from 'react';
import SubnetworksTable from "./SubnetworksTable";
import {Button, ButtonToolbar, Grid, PageHeader} from "react-bootstrap";
import AlertsHolder from "../../components/AlertsHolder";
import AddModal from "./AddModal";

class Subnetworks extends Component {

    state = {
        subnetworksTable: React.createRef(),
        alertsHolder: React.createRef(),
        isAddModalVisible: false
    };

    componentDidMount() {

    }

    render() {

        return <Grid>

            <PageHeader>
                Subnetworks
            </PageHeader>

            <AlertsHolder ref={this.state.alertsHolder}/>

            <ButtonToolbar>

                <Button bsStyle="primary"
                        onClick={() => this.setState({isAddModalVisible: true})}
                >
                    Add
                </Button>

                <Button
                    onClick={() => this.state.subnetworksTable.current.fetchItems()}
                >
                    reload
                </Button>

            </ButtonToolbar>

            <br/>

            <SubnetworksTable
                ref={this.state.subnetworksTable}
                addErrorAlert={(message) => this.state.alertsHolder.current.addAlert(message, "danger")}
                addWarningAlert={(message) => this.state.alertsHolder.current.addAlert(message, "warning")}
            />

            <AddModal
                show={this.state.isAddModalVisible}
                onHide={() => this.setState({isAddModalVisible: false})}
                onSuccess={() => {
                    this.setState({isAddModalVisible: false});
                    this.state.subnetworksTable.current.fetchItems();
                }}
                onError={(message) => {
                    this.setState({isAddModalVisible: false});
                    this.state.alertsHolder.current.addAlert(message, "danger")
                }}
            />
        </Grid>;
    }


}

export default Subnetworks;
