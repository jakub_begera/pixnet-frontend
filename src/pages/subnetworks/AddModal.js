import React, {Component} from 'react';
import {Button, ControlLabel, FormControl, FormGroup, Modal} from "react-bootstrap";
import Loader from "../../components/Loader";
import {buildHandlerUrl} from "../../utils/api";

class AddModal extends Component {

    state = {
        name: "",
        loading: false
    };

    getNameValidationState() {
        const length = this.state.name.length;
        if (length > 3) return 'success';
        else if (length > 0) return 'error';
        return null;
    }

    isFormValid() {
        return this.state.name.length > 3
    }

    async submit() {
        this.setState({loading: true});

        try {
            const response = await fetch(buildHandlerUrl("subnetwork/add"), {
                method: 'POST',
                body: JSON.stringify({
                    name: this.state.name
                }),
                headers: {
                    "Content-Type": "application/json; charset=utf-8"
                }
            });
            let json = await response.json();

            if (json && json.success !== true) {
                this.props.onError(`Adding of subnetwork was unsuccessful. (${json.message})`);
            } else {
                this.props.onSuccess();
            }
        } catch (e) {
            console.error("Adding of subnetwork was unsuccessful");
            this.props.onError(`Adding of subnetwork was unsuccessful. (${e.message})`);
        }

        this.setState({loading: false});
    }

    dismiss() {
        this.setState({loading: false});
        this.props.onHide()
    }

    render() {

        const isFormValid = this.isFormValid();

        return (
            <Modal
                {...this.props}
                bsSize="large"
                onHide={() => this.dismiss()}
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">Add subnetwork</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    {
                        this.state.loading
                            ? <Loader/>
                            : <form>
                                <FormGroup
                                    controlId="formBasicText"
                                    validationState={this.getNameValidationState()}
                                >
                                    <ControlLabel>Subnetwork name</ControlLabel>
                                    <FormControl
                                        type="text"
                                        value={this.state.value}
                                        placeholder="Enter name of new subnetwork"
                                        onChange={(e) => this.setState({name: e.target.value})}
                                    />
                                    <FormControl.Feedback/>
                                </FormGroup>
                            </form>
                    }

                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={() => this.dismiss()}>Cancel</Button>
                    <Button disabled={this.state.loading || !isFormValid} bsStyle="primary"
                            onClick={() => this.submit()}>Add</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default AddModal;