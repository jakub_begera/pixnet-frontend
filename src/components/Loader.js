import React, {Component} from 'react';
import {SyncLoader} from "react-spinners";
import {COLOR_ACCENT} from "../utils/colors";
import styled from "styled-components";

const Wrapper = styled.div`
  width: 100%;
  padding: 5em;
  text-align: center;
  
`;

const WrapperSmall = styled.div`
  width: 100%;
  padding: 1em;
  text-align: center;
  
`;

class Loader extends Component {

    render() {

        if (this.props.small) {
            return (
                <WrapperSmall>
                    <SyncLoader color={COLOR_ACCENT}/>
                </WrapperSmall>
            );
        }

        return (
            <Wrapper>
                <SyncLoader color={COLOR_ACCENT}/>
            </Wrapper>
        );
    }
}

export default Loader;
