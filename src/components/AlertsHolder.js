import React, {Component} from 'react';
import {Alert, Button} from "react-bootstrap";

class AlertsHolder extends Component {

    counter = 0;
    state = {
        items: []
    };

    addAlert(message, type = "danger") {
        const items = this.state.items;
        items.push({
            id: this.counter++,
            message: message,
            type: type
        });
        this.setState({
            items: items
        })
    }

    removeAlert(id) {
        this.setState({
            items: this.state.items.filter(item => item.id !== id)
        });
    }

    render() {
        return (
            this.state.items.map(item =>
                <Alert key={item.id} bsStyle={item.type}>
                    {item.message}
                    <br/>
                    <Button bsSize="xsmall" onClick={() => this.removeAlert(item.id)}>Close</Button>
                </Alert>
            )
        )
    }
}

export default AlertsHolder;
