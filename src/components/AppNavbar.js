import React, {Component} from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from 'react-router-bootstrap';

import styled from "styled-components";

const Img = styled.img`
  height: 2em;
  float: left;
  margin-top: -.5em;
  margin-right: 5px;
`;

class AppNavbar extends Component {
    render() {
        return (
            <Navbar inverse collapseOnSelect>
                <Navbar.Header>

                    <LinkContainer to="/" style={{cursor: 'pointer'}}>
                        <Navbar.Brand>
                            <span>PixNet</span>
                            <Img src={"/ic_icon.png"}/>
                        </Navbar.Brand>
                    </LinkContainer>

                    <Navbar.Toggle/>
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <LinkContainer to="/detectors">
                            <NavItem eventKey={1} href="/detectors">
                                Detectors
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to="/handlers">
                            <NavItem eventKey={2} href="/handlers">
                                Handlers
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to="/subnetworks">
                            <NavItem eventKey={2} href="/subnetworks">
                                Subnetworks
                            </NavItem>
                        </LinkContainer>

                    </Nav>

                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default AppNavbar;
